/**
### Temporarily keep this!
// TODO: Delete this later
Sample without AQI
{
    "location": {
        "name": "Bekasi",
        "region": "West Java",
        "country": "Indonesia",
        "lat": -6.23,
        "lon": 106.99,
        "tz_id": "Asia/Jakarta",
        "localtime_epoch": 1708678327,
        "localtime": "2024-02-23 15:52"
    },
    "current": {
        "last_updated_epoch": 1708677900,
        "last_updated": "2024-02-23 15:45",
        "temp_c": 32,
        "temp_f": 89.6,
        "is_day": 1,
        "condition": {
            "text": "Partly cloudy",
            "icon": "//cdn.weatherapi.com/weather/64x64/day/116.png",
            "code": 1003
        },
        "wind_mph": 12.5,
        "wind_kph": 20.2,
        "wind_degree": 310,
        "wind_dir": "NW",
        "pressure_mb": 1007,
        "pressure_in": 29.74,
        "precip_mm": 0,
        "precip_in": 0,
        "humidity": 75,
        "cloud": 50,
        "feelslike_c": 34.1,
        "feelslike_f": 93.3,
        "vis_km": 10,
        "vis_miles": 6,
        "uv": 11,
        "gust_mph": 17,
        "gust_kph": 27.4
    }
}

SAMPLE WITH AQI:
{
    "location": {
        "name": "Bekasi",
        "region": "West Java",
        "country": "Indonesia",
        "lat": -6.23,
        "lon": 106.99,
        "tz_id": "Asia/Jakarta",
        "localtime_epoch": 1708678304,
        "localtime": "2024-02-23 15:51"
    },
    "current": {
        "last_updated_epoch": 1708677900,
        "last_updated": "2024-02-23 15:45",
        "temp_c": 32,
        "temp_f": 89.6,
        "is_day": 1,
        "condition": {
            "text": "Partly cloudy",
            "icon": "//cdn.weatherapi.com/weather/64x64/day/116.png",
            "code": 1003
        },
        "wind_mph": 12.5,
        "wind_kph": 20.2,
        "wind_degree": 310,
        "wind_dir": "NW",
        "pressure_mb": 1007,
        "pressure_in": 29.74,
        "precip_mm": 0,
        "precip_in": 0,
        "humidity": 75,
        "cloud": 50,
        "feelslike_c": 34.1,
        "feelslike_f": 93.3,
        "vis_km": 10,
        "vis_miles": 6,
        "uv": 11,
        "gust_mph": 17,
        "gust_kph": 27.4,
        "air_quality": {
            "co": 1375.2,
            "no2": 48,
            "o3": 59.4,
            "so2": 40.5,
            "pm2_5": 44.9,
            "pm10": 57.1,
            "us-epa-index": 3,
            "gb-defra-index": 5
        }
    }
} 
*/

import { WindDirection } from "app/util/wind";
import { Alerts } from "./alert";

export interface Current {
    location: Location;
    current: CurrentDetail;
    alerts: Alerts;
}

export interface Condition {
    text: string;
    icon: string;
    code: number;
}

export interface CurrentDetail {
    last_updated_epoch: number;
    last_updated: string;
    temp_c: number; // temperature celcius
    temp_f: number; // temperature fahrenheit
    is_day: number; // is day
    condition: Condition;
    wind_mph: number;
    wind_kph: number;
    wind_degree: number; // wind direction in degree
    wind_dir: WindDirection; // wind direction as 16 point compass
    pressure_mb: number; // pressure in millibars
    pressure_in: number; // pressure in inches
    precip_mm: number; // Precipitation amount in millimeters
    precip_in: number; // Precipitation amount in inches
    humidity: number; // humidity as percentage
    cloud: number; // Cloud cover as percentage
    feelslike_c: number; // feels like temperature in celcius
    feelslike_f: number; // feels like temperature in fahrenheit
    vis_km: number; // visibility in kilometers
    vis_miles: number; // visibility in miles
    uv: number; // UV index
    gust_mph: number; // Wind gust in miles per hour
    gust_kph: number; // Wind gust in kilos per hour
    air_quality?: AirQuality;
}

export interface AirQuality {
    co: number; // Carbon monoxide (CO)
    no2: number; // Nitrogen dioxide (NO2)
    o3: number; // Ozone (O3)
    so2: number; // Sulfur dioxide (SO2)
    pm2_5: number; // Particulate matter (PM10 and PM2.5)
    pm10: number; // Particulate matter (PM10 and PM2.5)
    "us-epa-index": number;
    "gb-defra-index": number;
}
