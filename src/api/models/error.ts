export interface APIWeatherErrorServerResponse {
    error: APIWeatherErrorDetail;
}

export interface APIWeatherErrorDetail {
    code: number;
    message: string;
}
