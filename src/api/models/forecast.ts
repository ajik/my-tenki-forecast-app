import { AirQuality, Condition, Current } from "./current";
import { WindDirection } from "app/util/wind";

export interface Forecast extends Current {
    forecast: ForecastDetail;
}

export interface ForecastDetail {
    forecastday: ForecastDay[];
}

export interface ForecastDay {
    date: string;
    date_epoch: number;
    day: DayInformation;
    astro: AstronomyInformation;
}

export interface AstronomyInformation {
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phase: string;
    moon_illumination: number;
    is_moon_up: number;
    is_sun_up: number;
}

export interface HourInformation {
    time_epoch: number;
    last: string;
    temp_c: number; // temperature celcius
    temp_f: number; // temperature fahrenheit
    is_day: number; // is day
    condition: Condition;
    wind_mph: number;
    wind_kph: number;
    wind_degree: number; // wind direction in degree
    wind_dir: WindDirection; // wind direction as 16 point compass
    pressure_mb: number; // pressure in millibars
    pressure_in: number; // pressure in inches
    precip_mm: number; // Precipitation amount in millimeters
    precip_in: number; // Precipitation amount in inches
    snow_cm: number;
    humidity: number; // humidity as percentage
    cloud: number; // Cloud cover as percentage
    feelslike_c: number; // feels like temperature in celcius
    feelslike_f: number; // feels like temperature in fahrenheit
    windchill_c: number;
    windchill_f: number;
    heatindex_c: number;
    heatindex_f: number;
    dewpoint_c: number;
    dewpoint_f: number;
    daily_will_it_rain: number;
    daily_chance_of_rain: number;
    daily_will_it_snow: number;
    daily_chance_of_snow: number;
    vis_km: number; // visibility in kilometers
    vis_miles: number; // visibility in miles
    uv: number; // UV index
    gust_mph: number; // Wind gust in miles per hour
    gust_kph: number; // Wind gust in kilos per hour
    short_rad: number;
    diff_rad: number;
    air_quality?: AirQuality;
}

export interface DayInformation {
    maxtemp_c: number;
    maxtemp_f: number;
    mintemp_c: number;
    mintemp_f: number;
    avgtemp_c: number;
    avgtemp_f: number;
    maxwind_mph: number;
    maxwind_kph: number;
    totalprecip_mm: number;
    totalprecip_in: number;
    totalsnow_cm: number;
    avgvis_km: number;
    avgvis_miles: number;
    avghumidity: number;
    daily_will_it_rain: number;
    daily_chance_of_rain: number;
    daily_will_it_snow: number;
    daily_chance_of_snow: number;
    condition: Condition;
    uv: number;
}