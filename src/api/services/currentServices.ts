import { GetEndpointURL, GetEnvironmentVariableValue } from "app/util/system.util";
import axios from "axios";
import { NextRequest } from "next/server";
import { Current } from "../models/current";

export const getCurrentServices = async (req: NextRequest) => {
    try {
        let q = undefined;
        if (req.nextUrl.searchParams.get('q')) {
            q = req.nextUrl.searchParams.get('q')
        }
        if (q === undefined && req.headers.get('x-forwarded-for')) {
            q = req.headers.get('x-forwarded-for');
        }
        if (q === undefined || q?.includes("::1") || q?.includes('127.0.0.1') || q?.includes('localhost')) {
            if (GetEnvironmentVariableValue("NEXT_PUBLIC_API_ENV") === 'local') {
                q = "karawaci";
            } else {
                throw new Error("EM_0003");
            }
        }

        const url = GetEndpointURL();
        const key = GetEnvironmentVariableValue("NEXT_PUBLIC_API_KEY");
        const response = await axios.get(`${url}/current.json`, {
            params: { key, q, aqi: 'yes', alerts: 'yes' }
        });

        return response.data as Current;
    } catch (e) {
        console.log(e);
        throw e;
    }
}