import { getForecastServices } from "app/api/services/forecastServices";
import { SendErrorResponse, SendResponse } from "app/util/request-response";
import { NextRequest } from "next/server";

export async function GET(req: NextRequest) {

    // just for triggerring to build this page as dynamic page (server side)
    req.headers.get('referer')

    try {
        const response = await getForecastServices(req);
        return SendResponse(response);
    } catch (e: any) {
        console.error(`Failed get data from server, reason: ${e.message || e}`);
        return SendErrorResponse(e.message || e);
    }
}