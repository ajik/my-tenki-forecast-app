'use client';

import { useAppSelector, useAppStore } from "app/store/hooks";
import { appGlobalSelector } from "app/store/reducer/app-global/selector";
import Link from "next/link";

const ConfigurationPage = () => {
    const darkTheme = useAppSelector(appGlobalSelector.useDarkTheme);
    const lang = useAppSelector(appGlobalSelector.lang);

    return (
        <div>
            <p>Hello world!</p>
            <p>Use Dark Theme: {`${darkTheme}`}</p>
            <p>Lang: {lang}</p>
            <Link href={"/weather"}>
                To Weather
            </Link>
        </div>
    )
}

export default ConfigurationPage