'use client';

import { CardContent } from "app/components/shared/content/card-content";
import { TabMenu } from "app/components/shared/tab/tab-menu";
import { useAppSelector } from "app/store/hooks";
import { currentSelector } from "app/store/reducer/current/selector";
import { CurrentType } from "app/store/reducer/current/type";
import { ReactNode } from "react";
import { CurrentHook } from "./hooks";
import { AirQualityTab } from "app/components/pages/current/air-quality-tab";
import { LocationTab } from "app/components/pages/current/location-tab";
import { InfoTab } from "app/components/pages/current/info-tab";
import { SimpleCurrentInfo } from "app/components/shared/content/simple-current-info";

interface ComponentProps {
    children?: ReactNode;
}

export const CurrentCard = (props: ComponentProps) => {
    const { children } = props;
    const current = useAppSelector(currentSelector.current);

    const {
        activeTab,
        tabMenuLists,
        handleChangeActiveTab
    } = CurrentHook();

    return (
        <div className="w-full">
            <CardContent>
                <SimpleCurrentInfo
                    isDay={current?.current.is_day}
                    className="mb-2 tracking-tight text-gray-900 dark:text-white"
                    region={current?.location.region}
                    locationName={current?.location.name}
                    lastUpdated={current?.current.last_updated}
                    condition={{
                        icon: current?.current.condition.icon,
                        height: '100px',
                        width: '100px',
                        description: current?.current.condition.text,
                    }}
                    temperature={{
                        temp: current?.current.temp_c,
                        unit: "C",
                    }}
                />
                <div className="overflow-hidden">
                    <TabMenu
                        className='flex flex-nowrap flex-row overflow-x-hidden text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400'
                        lists={tabMenuLists}
                        onChangeActiveClass={handleChangeActiveTab}
                    />
                </div>
                <div className="mt-4">
                    <CurrentTab activeTab={activeTab} current={current} />
                </div>
                {children}
            </CardContent>
        </div>
    )
}

export interface CurrentTabProps {
    activeTab?: number;
    className?: string;
    current?: CurrentType | null;
}

const CurrentTab = (props: CurrentTabProps) => {
    const { activeTab, current } = props;
    if (activeTab === 1) {
        return (
            <AirQualityTab current={current} />
        )
    } else if (activeTab === 2) {
        return (
            <LocationTab current={current} />
        )
    } else if (activeTab === 0) {
        return (
            <InfoTab current={current} />
        )
    } else {
        return (
            <>{JSON.stringify(current?.current.air_quality, null, 4)}</>
        )
    }
}