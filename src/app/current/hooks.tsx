import { ListItemActiveTab } from "app/components/shared/tab/tab-menu";
import { useState } from "react";

export const CurrentHook = () => {
    const [activeTab, setActiveTab] = useState(0);

    const handleChangeActiveTab = (setActiveNumber: number) => {
        setActiveTab(setActiveNumber);
    }

    const ACTIVE_CLASS = "inline-block p-4 text-blue-600 bg-gray-100 rounded-t-lg active dark:bg-gray-800 dark:text-blue-500 hover:cursor-not-allowed";
    const INACTIVE_CLASS = "inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 dark:hover:bg-gray-800 dark:hover:text-gray-300 hover:cursor-pointer";

    const tabMenuLists: ListItemActiveTab[] = [
        {
            title: 'Info',
            className: 'me-2 flex-shrink-0',
            activeClassName: ACTIVE_CLASS,
            inactiveClassName: INACTIVE_CLASS,
        },
        {
            title: 'Dashboard',
            className: 'me-2',
            activeClassName: ACTIVE_CLASS,
            inactiveClassName: INACTIVE_CLASS,
        },
        {
            title: 'Location',
            className: 'me-2',
            activeClassName: ACTIVE_CLASS,
            inactiveClassName: INACTIVE_CLASS,
        },
    ]

    return {
        activeTab,
        tabMenuLists,
        handleChangeActiveTab
    }
}