'use client';

import { useAppDispatch } from "app/store/hooks";
import { CurrentCard } from "./component";
import { useEffect } from "react";
import { getCurrentAction } from "app/store/reducer/current/slice";

const CurrentPage = () => {

    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(
            getCurrentAction(undefined)
        )
    }, [])

    return (
        <div className="w-full">
            <CurrentCard />
        </div>
    )
}

export default CurrentPage