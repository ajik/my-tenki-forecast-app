import { CardContent } from "app/components/shared/content/card-content"
import { TabMenu } from "app/components/shared/tab/tab-menu";
import { useAppSelector } from "app/store/hooks";
import { forecastSelector } from "app/store/reducer/forecast/selector";
import { ForecastHook } from "./hooks";
import { ForecastTabContent } from "app/components/pages/forecast/forecast-tab-content";

export const ForecastComponent = () => {


    const forecastDays = useAppSelector(forecastSelector.forecastDays);

    const {
        activeTab,
        tabMenuLists,
        handleChangeActiveTab,
    } = ForecastHook(forecastDays);

    return (
        <div className="w-full">
            <CardContent>
                <div className="overflow-hidden">
                    <TabMenu
                        className='flex flex-nowrap flex-row overflow-x-hidden text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400'
                        lists={tabMenuLists}
                        onChangeActiveClass={handleChangeActiveTab}
                    />
                </div>
                <ForecastTabContent
                    activeTab={activeTab}
                    forecastDays={forecastDays}
                />
            </CardContent>
        </div>
    )
}