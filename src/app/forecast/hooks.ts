import { ListItemActiveTab } from "app/components/shared/tab/tab-menu";
import { ForecastDayType } from "app/store/reducer/forecast/type";
import dayjs from "dayjs";
import { useEffect, useState } from "react";


export const ForecastHook = (forecastDays: ForecastDayType[] | undefined) => {
    const [activeTab, setActiveTab] = useState(0);
    const [ tabMenuLists, setTabMenuLists] = useState<ListItemActiveTab[]>([]);

    const ACTIVE_CLASS = "inline-block p-4 text-blue-600 bg-gray-100 rounded-t-lg active dark:bg-gray-800 dark:text-blue-500 hover:cursor-not-allowed";
    const INACTIVE_CLASS = "inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 dark:hover:bg-gray-800 dark:hover:text-gray-300 hover:cursor-pointer";

    useEffect(() => {
        if(forecastDays) {
            const tabs: ListItemActiveTab[] = [];
            console.log(`forecastDays`, forecastDays);
            for(let forecastDay of forecastDays) {
                const tab: ListItemActiveTab = {
                    title: dayjs(forecastDay.date).format('MMM, DD YYYY'),
                    className: 'me-2',
                    activeClassName: ACTIVE_CLASS,
                    inactiveClassName: INACTIVE_CLASS,
                }
                tabs.push(tab);
            }
            setTabMenuLists(tabs);
            console.log(`tabs`, tabs);
        }
    }, [forecastDays])

    const handleChangeActiveTab = (activeTab: number) => {
        setActiveTab(activeTab);
    }

    return {
        activeTab,
        tabMenuLists,
        handleChangeActiveTab,
    }
}