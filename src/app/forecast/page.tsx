'use client';

import { useAppDispatch } from "app/store/hooks";
import { ForecastComponent } from "./component"
import { useEffect } from "react";
import { getForecastAction } from "app/store/reducer/forecast/slice";

const ForecastPage = () => {

    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(
            getForecastAction(undefined)
        )
    }, [])

    return (
        <div className="w-full">
            <ForecastComponent />
        </div>
    )
}

export default ForecastPage