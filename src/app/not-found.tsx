import { PrimaryBlueButton } from 'app/components/shared/button/button'
import { CardContent } from 'app/components/shared/content/card-content'
import Link from 'next/link'

export default function NotFound() {
    return (
        <div className='w-full'>
            <CardContent>
                <h1 className='text-gray-900 dark:text-white text-4xl m-3'>Not found – 404!</h1>
                <h3 className='m-3'>Sorry, your request page is not available!</h3>
                <PrimaryBlueButton className='m-3'>
                    <Link href="/current">Go back to Home</Link>
                </PrimaryBlueButton>
            </CardContent>
        </div>
    )
}