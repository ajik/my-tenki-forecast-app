'use client';

import { useAppSelector } from "app/store/hooks";
import { appGlobalSelector } from "app/store/reducer/app-global/selector";
import { ThemeProvider } from "next-themes";
import { useEffect } from "react";
import { useTheme } from "next-themes";
import { useRouter } from 'next/navigation';

export default function Home() {

    const darkTheme = useAppSelector(appGlobalSelector.useDarkTheme);
    const { setTheme } = useTheme();
    const { push } = useRouter();

    useEffect(() => {
        if (darkTheme) {
            setTheme('dark');
        } else {
            setTheme('light');
        }
        push('/current');
    }, [])

    return (
        <ThemeProvider attribute='class'>
            <></>
        </ThemeProvider>
    )
}
