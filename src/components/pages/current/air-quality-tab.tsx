import { CurrentTabProps } from "app/app/current/component";
import { CardContentSimpleListItem } from "app/components/shared/content/card-content";
import { useState } from "react";

const ACTIVE_CLASS = "inline-block p-4 text-blue-600 bg-gray-100 rounded-t-lg active dark:bg-gray-800 dark:text-blue-500 hover:cursor-not-allowed";
const INACTIVE_CLASS = "inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 dark:hover:bg-gray-800 dark:hover:text-gray-300 hover:cursor-pointer";

export const AirQualityTab = (props: CurrentTabProps) => {
    const { className, current } = props;
    const [activeAQTab, setActiveAQTab] = useState(1);

    const handleChangeActiveAQTab = (activeTab: number) => {
        setActiveAQTab(activeTab)
    }
    const AQInformation = () => {
        return (
            <div className="mt-3">
                <div className="mb-3">
                    <p className="text-2xl">Air Quality</p>
                </div>
                <hr />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="CO (Carbon Monoxide)"
                    value={`${current?.current.air_quality?.co} µg/m3`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="NO2 (Nitrogen Dioxide)"
                    value={`${current?.current.air_quality?.no2} µg/m3`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="O3 (Ozone)"
                    value={`${current?.current.air_quality?.o3} µg/m3`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="SO2 (Sulfure Dioxide)"
                    value={`${current?.current.air_quality?.so2} µg/m3`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="PM 2.5"
                    value={`${current?.current.air_quality?.pm2_5} µg/m3`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="PM 10"
                    value={`${current?.current.air_quality?.pm10} µg/m3`}
                />
            </div>
        )
    }
    return (
        <div className={`${className}`}>
            <div className="overflow-hidden">
                <ul className="flex flex-nowrap flex-row overflow-x-hidden text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400">
                    <li className="me-2 flex-shrink-0">
                        <a onClick={() => handleChangeActiveAQTab(1)} className={activeAQTab === 1 ? ACTIVE_CLASS : INACTIVE_CLASS}>Info</a>
                    </li>
                    <li className="me-2">
                        <a onClick={() => handleChangeActiveAQTab(2)} className={activeAQTab === 2 ? ACTIVE_CLASS : INACTIVE_CLASS}>What is this?</a>
                    </li>
                </ul>
            </div>
            {
                activeAQTab === 1 && <AQInformation />
            }
            {
                activeAQTab === 2 && <>Info Tab</>
            }
        </div>
    )
}