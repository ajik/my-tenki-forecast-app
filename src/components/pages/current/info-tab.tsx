import { CurrentTabProps } from "app/app/current/component";
import { CardContentSimpleListItem } from "app/components/shared/content/card-content";
import { UKDefraIndex, UKDefraIndexInformation, USEPAIndex, USEPAIndexInformation } from "app/util/air-index-pollution";
import { defraColorClassnameBasedOnBandValue } from "app/util/page-util";
import { getWindDirection } from "app/util/wind";
import { useEffect, useState } from "react";

export const InfoTab = (props: CurrentTabProps) => {
    const { className, current } = props;
    const [ defraIndexInfo, setDefraIndexInfo ] = useState<UKDefraIndexInformation>();
    const [ epaIndexInfo, setEpaIndexInfo ] = useState<USEPAIndexInformation>();

    useEffect(() => {

        if (current?.current.air_quality?.["gb-defra-index"] !== undefined) {
            const defraIndexValue = current?.current.air_quality?.["gb-defra-index"];
            const dataDefraIndex = UKDefraIndex
                .filter(e => e.index === defraIndexValue)
            if (dataDefraIndex) {
                setDefraIndexInfo(dataDefraIndex[0]);
            }
        }
        if(current?.current.air_quality?.["us-epa-index"]) {
            const epaIndexValue = current?.current.air_quality?.["us-epa-index"];
            const dataEpaIndex = USEPAIndex.filter(
                e => e.index === epaIndexValue
            )
            if(dataEpaIndex) {
                setEpaIndexInfo(dataEpaIndex[0]);
            }
        }
    }, [current])

    return (
        <div className={`${className}`}>
            <div>
                <div className="mb-3">
                    <p className="text-2xl">General Information</p>
                </div>
                <hr />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="Location"
                    value={`${current?.location.name}, ${current?.location.region}`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="Visibility"
                    value={`${current?.current.vis_km} km / ${current?.current.vis_miles} mile(s)`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="Wind Speed"
                    value={`${current?.current.wind_kph} kmh / ${current?.current.wind_mph} mph`}
                />
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn="Wind Direction"
                    value={`${getWindDirection(current?.current.wind_dir)} (${current?.current.wind_degree}º)`} 
                />
            </div>
            <div className="mt-3">
                <div className="mb-3">
                    <p className="text-2xl">Air Quality Information</p>
                </div>
                <hr />
                <div className="mt-5">
                    <p className="text-xl">UK Defra:</p>
                </div>
                <div className="flex flex-wrap flex-col pl-2">
                    <div className="mt-2">The concentration of an air pollutant: {defraIndexInfo?.microgramsPerCubicMeterAir}</div>
                    <div className="mt-2">
                        {defraIndexInfo?.generalMessage}
                    </div>
                    { 
                        defraIndexInfo && defraIndexInfo?.index > 0 ?
                            (
                                <CardContentSimpleListItem 
                                    className={`mt-3 ml-3`}
                                    keyColumnClassName={`${defraColorClassnameBasedOnBandValue(6)}`}
                                    keyColumn='Attention'
                                    value={`${defraIndexInfo?.forRisk}`}
                                />
                            ) : ''
                    }
                </div>
                <div className="mt-5">
                    <p className="text-xl">US EPA:</p>
                </div>
                <CardContentSimpleListItem 
                    className="mt-3 ml-3"
                    keyColumn={epaIndexInfo?.remark}
                    value={`${epaIndexInfo?.description}`}
                />
            </div>
        </div>
    )
}