import { CurrentTabProps } from "app/app/current/component";
import { CardContentSimpleListItem } from "app/components/shared/content/card-content";

export const LocationTab = (props: CurrentTabProps) => {
    const { className, current } = props;

    return (
        <div className={`${className}`}>
            <div className="mb-3">
                <p className="text-2xl">Detail Location</p>
            </div>
            <hr />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Name"
                value={`${current?.location.name}`}
            />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Region"
                value={`${current?.location.region}`}
            />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Country"
                value={`${current?.location.country}`}
            />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Coordinate"
                value={`${current?.location.lat}, ${current?.location.lon}`}
            />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Current Local Time"
                value={`(-+) ${current?.location.localtime}`}
            />
            <CardContentSimpleListItem 
                className="mt-3 ml-3"
                keyColumn="Timezone"
                value={`${current?.location.tz_id}`}
            />
        </div>
    )
}