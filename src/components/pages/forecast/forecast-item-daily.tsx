import { CardContentSimpleListItem } from "app/components/shared/content/card-content";
import { ConditionIcon } from "app/components/shared/content/condition-icon";
import { ForecastDayType } from "app/store/reducer/forecast/type";

interface ForecastItemDailyProps {
    className?: string;
    forecast?: ForecastDayType;
}

export const ForecastItemDaily = (props: ForecastItemDailyProps) => {
    const {className, forecast} = props;
    if(!forecast) return <></>;
    return (
        <div className={`p-3 ${className}`}>
            <div className="flex flex-col gap-3">
                <div className="flex flex-col">
                    <div>
                        <ConditionIcon 
                            className='mt-3 mr-3'
                            icon={forecast.day.condition.icon}
                            height={'50px'}
                            width={'50px'}
                        />
                    </div>
                    <small>{forecast.day.condition.text}</small>
                </div>
                <div>
                    <div className="flex flex-col">
                        <CardContentSimpleListItem 
                            className="mt-3 ml-3 w-full"
                            keyColumn="Humidity"
                            value={`${forecast.day.avghumidity}%`}
                        />
                        <CardContentSimpleListItem 
                            className="mt-3 ml-3 w-full"
                            keyColumn="Temperature Celcius"
                            value={`${forecast.day.avgtemp_c} C (${forecast.day.mintemp_c} ~ ${forecast.day.maxtemp_c})`}
                        />
                        <CardContentSimpleListItem 
                            className="mt-3 ml-3 w-full"
                            keyColumn="Temperature Fahrenheit"
                            value={`${forecast.day.avgtemp_f} F (${forecast.day.mintemp_f} ~ ${forecast.day.maxtemp_f})`}
                        />
                    </div>
                </div>
            </div>
        </div>
    )
}