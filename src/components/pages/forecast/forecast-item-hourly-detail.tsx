import { ConditionIcon } from "app/components/shared/content/condition-icon";
import { TemperatureUnitSimple } from "app/util/type/temperature-unit";
import dayjs from "dayjs";

interface ForecastItemHourlyDetailProp {
    key?: number | string;
    icon: string;
    conditionDescription: string;
    temperature?: number;
    temperatureUnit?: TemperatureUnitSimple;
    currentTime?: number;
    className?: string;
    humidity?: number;
    wind?: number;
    chanceOfRain?: number;
    chanceOfSnow?: number;
}

export const ForecastItemHourlyDetail = (prop: ForecastItemHourlyDetailProp) => {
    const { key, icon, conditionDescription, temperature, temperatureUnit, currentTime, className, humidity, wind, chanceOfRain, chanceOfSnow } = prop;
    return (
        <div
            key={key}
            className={ className ? className : 'border rounded-lg my-3 p-3'}
        >
            <div className="flex flex-col gap-3">
                <div className="flex flex-row">
                    <div className="flex flex-col w-1/6">
                        <div>
                            <ConditionIcon 
                                className='mt-1 mr-3'
                                icon={icon}
                                height={'50px'}
                                width={'50px'}
                            />
                        </div>
                        <small className="w-full break-word">{conditionDescription}</small>
                    </div>
                    <div className="mt-3 w-2/6">
                        <p className="text-3xl">{temperature}&deg; {temperatureUnit}</p>
                    </div>
                    <div className="w-3/6 text-right">
                        <small className='text-s'>{ currentTime ? dayjs.unix(currentTime).format('hh:mm a') : '---'}</small>
                    </div>
                    <hr />
                </div>
                <hr />
                <div className="flex flex-col gap-3">
                    <div className="flex flex-row">
                        <div className="w-1/2">Humidity {humidity}%</div>
                        <div className="w-1/2">Wind Speed {wind} kmh</div>
                    </div>
                    <div className="flex flex-row">
                        {chanceOfRain && chanceOfRain > 0 ? <div className="w-1/2">Chance of rain {chanceOfRain}%</div> : ''}
                        {chanceOfSnow && chanceOfSnow > 0 ? <div className="w-1/2">Chance of snow {chanceOfSnow}%</div> : '' }
                    </div>
                </div>
            </div>
        </div>
    )
}