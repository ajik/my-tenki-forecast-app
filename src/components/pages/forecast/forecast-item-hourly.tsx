import { ConditionIcon } from "app/components/shared/content/condition-icon";
import { ForecastDayType, ForecastHourlyDetailType } from "app/store/reducer/forecast/type";
import dayjs from "dayjs";
import { useEffect, useState } from "react";
import { ForecastItemHourlyDetail } from "./forecast-item-hourly-detail";

interface ForecastItemHourlyProps {
    forecast?: ForecastDayType;
}

export const ForecastItemHourly = (props: ForecastItemHourlyProps) => {
    const {forecast} = props;
    const [ forecastHourly, setForecastHourly] = useState<ForecastHourlyDetailType[] | undefined>()

    useEffect(() => {
        if(forecast && forecast.hour) {
            setForecastHourly(forecast.hour)
        }
    }, [forecast])

    const now = dayjs();

    return (
        <div>
            {
                forecastHourly && forecastHourly.map((item, index) =>  {
                    const itemDate = dayjs.unix(item.time_epoch);
                    if(itemDate.isSame(now, 'day')) {
                        if(itemDate.isBefore(now, 'minute')) {
                            return null;
                        }
                    }
                    return (
                        <div key={index}>
                            <ForecastItemHourlyDetail 
                                icon={item.condition.icon}
                                conditionDescription={item.condition.text}
                                currentTime={item.time_epoch}
                                temperature={item.temp_c}
                                temperatureUnit='C'
                                className='border rounded-lg my-3 p-3'
                                humidity={item.humidity}
                                wind={item.wind_kph}
                                chanceOfRain={item.chance_of_rain}
                                chanceOfSnow={item.chance_of_snow}
                            />
                        </div>
                    )
                })
            }
        </div>
    )
}