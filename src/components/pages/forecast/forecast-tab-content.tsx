import { ForecastDayType } from "app/store/reducer/forecast/type";
import { useEffect, useState } from "react";
import { ForecastItemDaily } from "./forecast-item-daily";
import { TabMenu } from "app/components/shared/tab/tab-menu";
import { ForecastItemHourly } from "./forecast-item-hourly";

interface ForecastTabContentProps {
    activeTab: number;
    forecastDays: ForecastDayType[] | undefined;
}

export const ForecastTabContent = (props: ForecastTabContentProps) => {

    const {activeTab, forecastDays} = props;
    const [ forecastDisplayed, setForecastDisplayed ] = useState<ForecastDayType | undefined>();
    const [ periodTab, setPeriodTab] = useState<number>(0);

    const ACTIVE_CLASS = "inline-block p-4 text-blue-600 bg-gray-100 rounded-t-lg active dark:bg-gray-800 dark:text-blue-500 hover:cursor-not-allowed";
    const INACTIVE_CLASS = "inline-block p-4 rounded-t-lg hover:text-gray-600 hover:bg-gray-50 dark:hover:bg-gray-800 dark:hover:text-gray-300 hover:cursor-pointer";

    const activeTabList = [
        {
            title: 'Daily',
            className: 'me-2',
            activeClassName: ACTIVE_CLASS,
            inactiveClassName: INACTIVE_CLASS,
        },{
            title: 'Hourly',
            className: 'me-2',
            activeClassName: ACTIVE_CLASS,
            inactiveClassName: INACTIVE_CLASS,
        }
    ]

    useEffect(() => {
        if(activeTab !== undefined && forecastDays) {
            const forecast = forecastDays[activeTab] || undefined;
            setForecastDisplayed(forecast);
            setPeriodTab(0);
        }
    }, [activeTab])

    const handleChangeActiveTab = (tab: number) => {
        setPeriodTab(tab);
    }

    if(!forecastDisplayed) {
        return <>Data Not Found</>;
    }
    
    return (
        <div>
            <ul 
                className='flex flex-nowrap flex-row overflow-x-hidden text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:border-gray-700 dark:text-gray-400'
            >
                {
                    activeTabList.map((item, index) => (
                        <li className={item.className} key={index}>
                            <a 
                                onClick={() => handleChangeActiveTab(index)}
                                className={periodTab === index ? item.activeClassName : item.inactiveClassName}
                            >
                                {item.title}
                            </a>
                        </li>
                    ))
                }
            </ul>
            {
                periodTab === 0 && (
                    <ForecastItemDaily
                        className=''
                        forecast={forecastDisplayed}
                    />
                )
            }
            {
                periodTab === 1 && (
                    <ForecastItemHourly 
                        forecast={forecastDisplayed}
                    />
                ) 
            }
        </div>
    )
}