import { ReactNode } from "react";

export type ButtonType = "submit" | "button" | "reset";

interface ButtonProp {
    children?: ReactNode;
    onClick?: any;
    disabled?: boolean;
    className?: string;
    "data-testid"?: string;
    type?: ButtonType;
}

export const PrimaryBlueButton = (prop: ButtonProp) => {
    const { children, onClick, disabled, className, type } = prop;
    return (
        <button
            onClick={onClick}
            disabled={disabled}
            className={`${className ? className : ""} bg-blue-500 px-4 py-2 font-semibold rounded dark:text-gray-900 text-white hover:bg-blue-600`}
            data-testid={prop["data-testid"] ? prop["data-testid"] : undefined}
            type={type ? type : "button"}
        >
            {children}
        </button>
    ); 
};