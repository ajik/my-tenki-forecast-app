"use client";

import { ReactNode } from "react";

interface CardContentProps {
    children?: ReactNode
}

export const CardContent = (props: CardContentProps) => {
    const { children } = props;
    return (
        <div className="block w-full flex p-6 my-8 mx-4 bg-[#fafaf5] border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <div className="font-normal flex-1 text-gray-700 dark:text-gray-400">
                {children}
            </div>
        </div>
    )
}

interface CardContentSimpleListItemProps {
    className?: string;
    keyColumn?: string;
    keyColumnClassName?: string;
    valueClassName?: string;
    value?: string;
}

export const CardContentSimpleListItem = (props: CardContentSimpleListItemProps) => {
    const {className = '', keyColumn = '', value = '', keyColumnClassName = '', valueClassName = ''} = props;
    return (
        <div className={`flex flex-wrap flex-row ${className}`}>
            <div className={`${keyColumnClassName} lg:w-2/6 w-3/6`}>
                <p className={`${keyColumnClassName}`}>{keyColumn}</p>
            </div>
            <div className="lg:w-4/6 w-3/6">
                <div className="flex flex-row">
                    <div className="mr-3">:</div>
                    <div className={`${valueClassName}`}>{ value }</div>
                </div>
            </div>
        </div>
    )

}