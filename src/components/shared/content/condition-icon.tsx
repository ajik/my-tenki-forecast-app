
interface ConditionIconProps {
    icon?: string;
    height?: string | number;
    width?: string | number;
    className?: string;
}

export const ConditionIcon = (props: ConditionIconProps) => {
    const {icon, height, width, className} = props;
    const DEFAULT_CLASSNAME = 'w-3/6 content-right flex justify-end'
    return (
        <div className={className ? className : DEFAULT_CLASSNAME}>
            <div>
                <img
                    src={icon}
                    height={height ? height : '100px'}
                    width={width ? width : '100px'}
                />
            </div>
        </div>
    )
}