'use client';

import { ReactNode } from "react";

interface ContentProps {
    children?: ReactNode
}

export const MainContent = (prop: ContentProps) => {
    const { children } = prop;

    return (
        <div className="flex justify-center mt-20">
            <div className="shrink md:w-4/6 w-full ml-0 md:ml-24 mr-5">
                <div className="flex justify-center mt-5">
                    {children}
                </div>
            </div>
            <div className="flex-auto invisible md:w-2/6 w-0/6">
                <div className="flex justify-center mt-5">
                    03
                </div>
            </div>
        </div>
    )
}