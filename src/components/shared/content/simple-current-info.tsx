import { TemperatureUnitSimple } from "app/util/type/temperature-unit";
import { ConditionIcon } from "./condition-icon";

interface CurrentConditionProps {
    icon?: string;
    height?: string | number;
    width?: string | number;
    description?: string;
}

interface TemperatureProps {
    temp?: number | string;
    unit: TemperatureUnitSimple;
}

interface SimpleCurrentInfoProps {
    className?: string;
    locationName?: string;
    region?: string;
    lastUpdated?: string;
    condition: CurrentConditionProps;
    temperature?: TemperatureProps;
    isDay?: number;

}


export const SimpleCurrentInfo = (props: SimpleCurrentInfoProps) => {
    const {className, locationName, region, lastUpdated, condition, temperature, isDay } = props;
    return (
        <div
            className={`text-center font-bold ${className}`}
        >
            <p className="text-2xl">{locationName ? locationName : '-'}, {region ? region : '-'}</p>
            <p className="text-sm">{lastUpdated}</p>
            <div className="flex flex-row content-center w-full mt-5">
                <ConditionIcon 
                    className='w-3/6 content-right flex justify-end'
                    icon={condition.icon}
                    width='100px'
                    height='100px'
                />
                <div className="w-3/6 text-left flex flex-col justify-center">
                    <p className="text-4xl">{temperature?.temp ? temperature?.temp : '-'} &deg;{temperature?.unit}</p>
                    <p>{ isDay ? 'Noon' : 'Night'}, {condition.description}</p>
                </div>
            </div>
        </div>
    )
}