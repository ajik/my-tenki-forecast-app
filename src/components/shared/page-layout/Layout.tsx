import { ReactNode } from "react"
import { ResponsiveNavbar } from "../navbar/navbar";
import { MainContent } from "../content/content";

interface LayoutProps {
    children?: ReactNode
}


export const Layout = (props: LayoutProps) => {
    const { children } = props;
    const defaultFont = 'Consolas';
    return (
        <div className={`font-[${defaultFont}]`}>
            <ResponsiveNavbar />
            <MainContent>
                { children }
            </MainContent>
        </div>
    )
}