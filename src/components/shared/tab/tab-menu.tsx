import { useState } from "react";


export interface ListItemActiveTab {
    title: string;
    className: string;
    activeClassName: string;
    inactiveClassName: string;
}


export interface TabMenuProps {
    className?: string;
    lists: ListItemActiveTab[];
    onChangeActiveClass?: (activeTab: number) => void;
}

export const TabMenu = (props: TabMenuProps) => {
    const {className, lists, onChangeActiveClass} = props;
    const [activeTab, setActiveTab] = useState(0);
    const handleChangeActiveTab = (tabTobeActivated: number) => {
        setActiveTab(tabTobeActivated);
        if(onChangeActiveClass) {
            onChangeActiveClass(tabTobeActivated);
        }
    }
    return (
        <ul className={className}>
            {
                lists.map((item, index) => (
                    <li className={item.className} key={index}>
                        <a 
                            onClick={() => handleChangeActiveTab(index)}
                            className={activeTab === index ? item.activeClassName : item.inactiveClassName}
                        >
                            {item.title}
                        </a>
                    </li>
                ))
            }
        </ul>
    )
}