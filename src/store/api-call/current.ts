import axios from "axios"
import { CurrentType } from "../reducer/current/type";

export const getCurrentApi = async (q: string | undefined) => {
    const response = await axios.get('api/weather/current', {
        params: { q }
    })

    return response.data as CurrentType;
}