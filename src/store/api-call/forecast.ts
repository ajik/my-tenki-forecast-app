import axios from "axios"
import { ForecastType } from "../reducer/forecast/type";

export const getForecastApi = async (q: string | undefined) => {
    const response = await axios.get('api/weather/forecast', {
        params: { q }
    })

    console.log(response.data);

    return response.data as ForecastType;
}