import { fork } from "@redux-saga/core/effects"
import { setDarkModeTehemeSagaWatcher } from "./set-dark-mode.saga"

export const appGlobalSaga = [
    fork(setDarkModeTehemeSagaWatcher)
]