import { put, takeLatest } from "redux-saga/effects";
import { setDarkThemeSuccessAction } from "../slice";
import { PayloadAction } from "@reduxjs/toolkit";

function* setDarkModeThemeSaga({ payload }: PayloadAction<boolean>) {
    yield put(setDarkThemeSuccessAction(payload));
}

export function* setDarkModeTehemeSagaWatcher() {
    yield takeLatest("app-global/setDarkThemeAction", setDarkModeThemeSaga);
}