import { StoreStateType } from "../../store.type";

export const appGlobalSelector = {
    useDarkTheme: (state: StoreStateType): boolean => state.appGlobal.darkTheme,
    lang: (state: StoreStateType): string => state.appGlobal.lang,
};
