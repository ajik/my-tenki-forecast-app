import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { AppGlobalTypeState } from "./type";
import storage from "redux-persist/lib/storage";

const appGlobalInitialState: AppGlobalTypeState = {
    lang: "en",
    darkTheme: false,
}

export const appGlobalPersistConfig = {
    key: "app-global",
    storage,
};

export const appGlobalSlice = createSlice({
    name: "app-global",
    initialState: appGlobalInitialState,
    reducers: {
        setDarkThemeAction: (
            _state: AppGlobalTypeState, 
            { payload: _isDarkThemeApplied }: PayloadAction<boolean>
        ) => { },
        setDarkThemeSuccessAction: (
            state: AppGlobalTypeState, 
            { payload: isDarkThemeApplied }: PayloadAction<boolean>
        ) => {
            state.darkTheme = isDarkThemeApplied;
            state.lang = 'xxx';
        },
    }
})

export const {
    setDarkThemeAction,
    setDarkThemeSuccessAction,
} = appGlobalSlice.actions

export default appGlobalSlice.reducer