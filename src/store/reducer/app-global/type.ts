export interface AppGlobalTypeState {
    lang: string,
    darkTheme: boolean,
}