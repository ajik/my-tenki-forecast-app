import { put, takeLatest } from "redux-saga/effects";
import { PayloadAction } from "@reduxjs/toolkit";
import { getCurrentApi } from "app/store/api-call/current";
import { getCurrentFailAction, getCurrentSuccessAction } from "../slice";
import { CurrentType } from "../type";

function* getCurrentSaga({ payload }: PayloadAction<string | undefined>) {
    try {
        const current: CurrentType = yield getCurrentApi(payload);
        yield put(getCurrentSuccessAction(current));
    } catch (e: any) {
        yield put(getCurrentFailAction(e.message || e))
    }
}

export function* getCurrentSagaWatcher() {
    yield takeLatest("current/getCurrentAction", getCurrentSaga);
}