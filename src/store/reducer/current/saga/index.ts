import { fork } from "@redux-saga/core/effects"
import { getCurrentSagaWatcher } from "./get-current.saga"

export const currentSaga = [
    fork(getCurrentSagaWatcher)
]