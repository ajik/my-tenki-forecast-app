import { StoreStateType } from "../../store.type";
import { CurrentType } from "./type";

export const currentSelector = {
    current: (state: StoreStateType): CurrentType | null => state.current.current,
    isError: (state: StoreStateType): boolean => state.current.isError,
    isLoading: (state: StoreStateType): boolean => state.current.isLoading,
};
