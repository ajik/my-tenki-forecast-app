import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { CurrentType, CurrentTypeState } from "./type";

const currentInitialState: CurrentTypeState = {
    current: null,
    isLoading: false,
    isError: false,
    errorMessage: null
}

export const currentSlice = createSlice({
    name: "current",
    initialState: currentInitialState,
    reducers: {
        getCurrentAction: (
            state: CurrentTypeState,
            { payload: q }: PayloadAction<string | undefined>
        ) => {
            state.keySearch = q;
            state.isLoading = true;
            state.isError = false;
            state.errorMessage = null;
        },
        getCurrentSuccessAction: (
            state: CurrentTypeState,
            { payload: current }: PayloadAction<CurrentType>
        ) => {
            state.isLoading = false;
            state.isError = false;
            state.errorMessage = null;
            state.current = current;
        },
        getCurrentFailAction: (
            state: CurrentTypeState,
            { payload: error }: PayloadAction<any>
        ) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = error.message || error;
            state.current = null;
        }
    }
})

export const {
    getCurrentAction,
    getCurrentSuccessAction,
    getCurrentFailAction,
} = currentSlice.actions

export default currentSlice.reducer