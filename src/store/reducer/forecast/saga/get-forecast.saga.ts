import { put, takeLatest } from "redux-saga/effects";
import { PayloadAction } from "@reduxjs/toolkit";
import { getForecastApi } from "app/store/api-call/forecast";
import { ForecastType } from "../type";
import { getForecastFailAction, getForecastSuccessAction } from "../slice";

function* getForecastSaga({ payload }: PayloadAction<string | undefined>) {
    try {
        const current: ForecastType = yield getForecastApi(payload);
        yield put(getForecastSuccessAction(current));
    } catch (e: any) {
        yield put(getForecastFailAction(e.message || e))
    }
}

export function* getForecastSagaWatcher() {
    yield takeLatest("forecast/getForecastAction", getForecastSaga);
}