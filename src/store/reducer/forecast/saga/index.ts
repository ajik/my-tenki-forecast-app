import { fork } from "@redux-saga/core/effects"
import { getForecastSagaWatcher } from "./get-forecast.saga"

export const forecastSaga = [
    fork(getForecastSagaWatcher)
]