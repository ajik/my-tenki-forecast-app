import { StoreStateType } from "../../store.type";
import { ForecastDayType } from "./type";

export const forecastSelector = {
    forecastDays: (state: StoreStateType): ForecastDayType[] | undefined => state.forecast.forecast?.forecast?.forecastday,
    isError: (state: StoreStateType): boolean => state.current.isError,
    isLoading: (state: StoreStateType): boolean => state.current.isLoading,
    alerts: (state: StoreStateType) => state.forecast.forecast?.alerts,
};
