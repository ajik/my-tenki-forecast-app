import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { ForecastType, ForecastTypeState } from "./type";

const forecastInitialState: ForecastTypeState = {
    forecast: null,
    isLoading: false,
    isError: false,
    errorMessage: null
}

export const forecastSlice = createSlice({
    name: 'forecast',
    initialState: forecastInitialState,
    reducers: {
        getForecastAction: (
            state: ForecastTypeState,
            { payload: q }: PayloadAction<string | undefined>
        ) => {
            state.keySearch = q;
            state.isLoading = true;
            state.isError = false;
            state.errorMessage = null;
        },
        getForecastSuccessAction: (
            state: ForecastTypeState,
            { payload: forecast }: PayloadAction<ForecastType>
        ) => {
            state.isLoading = false;
            state.isError = false;
            state.errorMessage = null;
            state.forecast = forecast;
        },
        getForecastFailAction: (
            state: ForecastTypeState,
            { payload: error }: PayloadAction<any>
        ) => {
            state.isLoading = false;
            state.isError = true;
            state.errorMessage = error.message || error;
            state.forecast = null;
        }
    }
})

export const {
    getForecastAction,
    getForecastSuccessAction,
    getForecastFailAction,
} = forecastSlice.actions

export default forecastSlice.reducer