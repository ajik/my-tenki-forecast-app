import { WindDirection } from "app/util/wind";

export type ForecastTypeState = {
    forecast: ForecastType | null;
    isLoading: boolean;
    isError: boolean;
    errorMessage: string | any | null;
    keySearch?: string;
}

export type ForecastType = {
    location: ForecastLocationType;
    current: ForecastCurrentDetailType;
    alerts: ForecastAlertsType;
    forecast: ForecastListDaysType;
}

export type ForecastListDaysType = {
    forecastday: ForecastDayType[];
}

export type ForecastDayType = {
    date: string;
    date_epoch: number;
    day: DayInformationType;
    astro: ForecastAstronomyInformationType;
    hour: ForecastHourlyDetailType[];
}

export type DayInformationType = {
    maxtemp_c: number;
    maxtemp_f: number;
    mintemp_c: number;
    mintemp_f: number;
    avgtemp_c: number;
    avgtemp_f: number;
    maxwind_mph: number;
    maxwind_kph: number;
    totalprecip_mm: number;
    totalprecip_in: number;
    totalsnow_cm: number;
    avgvis_km: number;
    avgvis_miles: number;
    avghumidity: number;
    daily_will_it_rain: number;
    daily_chance_of_rain: number;
    daily_will_it_snow: number;
    daily_chance_of_snow: number;
    condition: ForecastConditionType;
    uv: number;
}

export type ForecastAstronomyInformationType = {
    sunrise: string;
    sunset: string;
    moonrise: string;
    moonset: string;
    moon_phase: string;
    moon_illumination: number;
    is_moon_up: number;
    is_sun_up: number;
}

export type ForecastLocationType = {
    name: string;
    region: string;
    country: string;
    lat: number;
    lon: number;
    tz_id: string;
    localtime_epoch: number;
    localtime: string;
}

export type ForecastConditionType = {
    text: string;
    icon: string;
    code: number;
}

export type ForecastCurrentDetailType = {
    last_updated_epoch: number;
    last_updated: string;
    temp_c: number;
    temp_f: number;
    is_day: number;
    condition: ForecastConditionType;
    wind_mph: number;
    wind_kph: number;
    wind_degree: number;
    wind_dir: WindDirection;
    pressure_mb: number;
    pressure_in: number;
    precip_mm: number;
    precip_in: number;
    humidity: number;
    cloud: number;
    feelslike_c: number;
    feelslike_f: number;
    vis_km: number;
    vis_miles: number;
    uv: number;
    gust_mph: number;
    gust_kph: number;
    air_quality?: ForecastAirQuality;
}

export type ForecastAirQuality = {
    co: number;
    no2: number;
    o3: number;
    so2: number;
    pm2_5: number;
    pm10: number;
    "us-epa-index": number;
    "gb-defra-index": number;
}

export type ForecastAlertsType = {
    alert: ForecastAlertDetailType[];
}

export type ForecastAlertDetailType = {
    headline: string;
    msgtype: string;
    severity: string;
    urgency: string;
    areas: string;
    category: string;
    certainty: string;
    event: string;
    note: string;
    effective: string;
    expires: string;
    desc: string;
}

export type ForecastHourlyDetailType = {
    time_epoch: number;
    time: string;
    temp_c: number;
    temp_f: number;
    is_day: number;
    condition: ForecastConditionType;
    wind_mph: number;
    wind_kph: number;
    wind_degree: number;
    wind_dir: WindDirection;
    pressure_mb: number;
    pressure_in: number;
    precip_mm: number;
    precip_in: number;
    snow_cm: number;
    humidity: number;
    cloud: number;
    feelslike_c: number;
    feelslike_f: number;
    windchill_c: number;
    windchill_f: number;
    heatindex_c: number;
    heatindex_f: number;
    dewpoint_c: number;
    dewpoint_f: number;
    will_it_rain: number;
    chance_of_rain: number;
    will_it_snow: number;
    chance_of_snow: number;
    vis_km: number;
    vis_miles: number;
    uv: number;
    gust_mph: number;
    gust_kph: number;
}