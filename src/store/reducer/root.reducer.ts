import { combineReducers } from "redux";
import { AppGlobalTypeState } from "./app-global/type";
import appGlobalReducer, { appGlobalPersistConfig } from "./app-global/slice";
import persistReducer from "redux-persist/es/persistReducer";
import currentReducer from "./current/slice";
import forecastReducer from "./forecast/slice";
import { CurrentTypeState } from "./current/type";
import { ForecastTypeState } from "./forecast/type";

export type StateType = {
    appGlobal: AppGlobalTypeState;
    current: CurrentTypeState;
    forecast: ForecastTypeState;
};

const rootReducers = combineReducers({
    appGlobal: persistReducer(appGlobalPersistConfig, appGlobalReducer),
    current: currentReducer,
    forecast: forecastReducer,
});

export default rootReducers;