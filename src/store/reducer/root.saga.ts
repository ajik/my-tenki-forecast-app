import { all } from "redux-saga/effects";
import { appGlobalSaga } from "./app-global/saga";
import { currentSaga } from "./current/saga";
import { forecastSaga } from "./forecast/saga";

const rootSaga = function* () {
    yield all([
        ...appGlobalSaga,
        ...currentSaga,
        ...forecastSaga,
    ]);
};

export default rootSaga;