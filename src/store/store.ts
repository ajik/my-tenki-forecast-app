import { Store, configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import logger from "redux-logger";
import rootReducer from "./reducer/root.reducer";
import rootSaga from "./reducer/root.saga";

import storage from 'redux-persist/lib/storage';
import {
    persistStore,
    persistReducer,
    REHYDRATE,
    PERSIST,
} from 'redux-persist'

const persistConfig = {
    key: 'root',
    storage,
};

const isClient = typeof window !== 'undefined';
const sagaMiddleware = createSagaMiddleware();

let store: Store

if (isClient) {
    const persistedReducer = persistReducer(persistConfig, rootReducer);
    store = configureStore({
        reducer: persistedReducer,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [
                    REHYDRATE, 
                    PERSIST,
                ],
            },
        }).concat([
            logger,
            sagaMiddleware]),
    });
} else {
    store = configureStore({
        reducer: rootReducer,
        middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat([
            logger,
            sagaMiddleware]),
    })
}

export const makeStore = () => {
    return store
}
sagaMiddleware.run(rootSaga);

export default store;
export type AppStore = ReturnType<typeof makeStore>
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
export const persistor = persistStore(store);