import rootReducer from './reducer/root.reducer';

export type StoreStateType = ReturnType<typeof rootReducer>;