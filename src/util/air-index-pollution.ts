export interface USEPAIndexInformation {
    index: number;
    remark: USEPAStandard;
    description: string;
}

type USEPAStandard = "Good" | "Moderate" | "Unhealthy for sensitive group" | "Unhealthy" | "Very unhealthy" | "Hazardous";

export const USEPAIndex: USEPAIndexInformation[] = [
    {
        index: 1,
        remark: "Good",
        description: "Air quality is good and poses little or no risk.",
    },
    {
        index: 2,
        remark: "Moderate",
        description: "Air quality is acceptable; however, there may be some health concern for a small number of unusually sensitive people. While EPA cannot identify these people, studies indicate that there are people who experience health effects when air quality is in the moderate range."
    },
    {
        index: 3,
        remark: "Unhealthy for sensitive group",
        description: "When air quality is in this range, people who are in sensitive groups, whether the increased risk is due to medical conditions, exposure conditions, or innate susceptibility, may experience health effects when engaged in outdoor activities. However, exposures to ambient concentrations in this range are not likely to result in effects in the general population. For particle pollution, the sensitive groups include people with heart and lung disease, older adults, children, people with diabetes, and people of lower SES.",
    },
    {
        index: 4,
        remark: "Unhealthy",
        description: "When air quality is in this range, everyone who is active outdoors may experience effects. Members of sensitive groups are likely to experience more serious effects.",
    },
    {
        index: 5,
        remark: "Very unhealthy",
        description: "When air quality is in this range, it is expected that there will be widespread effects among the general population and more serious effects in members of sensitive groups."
    },
    {
        index: 6,
        remark: "Hazardous",
        description: "Air quality in this range triggers health warnings of emergency conditions by media outlets. The entire population is more likely to be affected by serious health effects.",
    },
]

export interface UKDefraIndexInformation {
    index: number;
    band: UKDefraIndexBand;
    microgramsPerCubicMeterAir: string; // micrograms (one-millionth of a gram) per cubic meter air or µg/m3.
    generalMessage: string;
    forRisk: string;
}

type UKDefraIndexBand = "Low" | "Moderate" | "High" | "Very High";

export const UKDefraIndex: UKDefraIndexInformation[] = [
    {
        index: 1,
        band: "Low",
        microgramsPerCubicMeterAir: "0-11",
        forRisk: "Enjoy your usual outdoor activities.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 2,
        band: "Low",
        microgramsPerCubicMeterAir: "12-23",
        forRisk: "Enjoy your usual outdoor activities.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 3,
        band: "Low",
        microgramsPerCubicMeterAir: "24-35",
        forRisk: "Enjoy your usual outdoor activities.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 4,
        band: "Moderate",
        microgramsPerCubicMeterAir: "36-41",
        forRisk: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 5,
        band: "Moderate",
        microgramsPerCubicMeterAir: "42-47",
        forRisk: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 6,
        band: "Moderate",
        microgramsPerCubicMeterAir: "48-53",
        forRisk: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
        generalMessage: "Enjoy your usual outdoor activities.",
    },
    {
        index: 7,
        band: "High",
        microgramsPerCubicMeterAir: "54-58",
        forRisk: "Adults and children with lung problems, and adults with heart problems, should reduce strenuous physical exertion, particularly outdoors, and particularly if they experience symptoms. People with asthma may find they need to use their reliever inhaler more often. Older people should also reduce physical exertion.",
        generalMessage: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
    },
    {
        index: 8,
        band: "Moderate",
        microgramsPerCubicMeterAir: "59-64",
        forRisk: "Adults and children with lung problems, and adults with heart problems, should reduce strenuous physical exertion, particularly outdoors, and particularly if they experience symptoms. People with asthma may find they need to use their reliever inhaler more often. Older people should also reduce physical exertion.",
        generalMessage: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
    },
    {
        index: 9,
        band: "Moderate",
        microgramsPerCubicMeterAir: "65-70",
        forRisk: "Adults and children with lung problems, and adults with heart problems, should reduce strenuous physical exertion, particularly outdoors, and particularly if they experience symptoms. People with asthma may find they need to use their reliever inhaler more often. Older people should also reduce physical exertion.",
        generalMessage: "Adults and children with lung problems, and adults with heart problems, who experience symptoms, should consider reducing strenuous physical activity, particularly outdoors.",
    },
    {
        index: 10,
        band: "Very High",
        microgramsPerCubicMeterAir: "71 or more",
        forRisk: "Adults and children with lung problems, adults with heart problems, and older people, should avoid strenuous physical activity. People with asthma may find they need to use their reliever inhaler more often.",
        generalMessage: "Reduce physical exertion, particularly outdoors, especially if you experience symptoms such as cough or sore throat.",
    },
]
