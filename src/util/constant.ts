import { HttpStatusCode } from "axios";

export const SERVER_ERROR_MESSAGE = {
    "EM_0000": {
        code: "EM_0000",
        message: "Unknown Error",
        responseCode: HttpStatusCode.InternalServerError,
    },
    "EM_0001": {
        code: "EM_0001",
        message: "Environment variable is not registered!",
        responseCode: HttpStatusCode.InternalServerError,
    },
    "EM_0002": {
        code: "EM_0002",
        message: "Rate limit exceeded!",
        responseCode: HttpStatusCode.TooManyRequests,
    },
    "EM_0003": {
        code: "EM_0003",
        message: "Missing parameter!",
        responseCode: HttpStatusCode.BadRequest,
    },
}

export const TOKEN_LIMIT = 10;

export type YesOrNo = "yes" | "no" | undefined;
