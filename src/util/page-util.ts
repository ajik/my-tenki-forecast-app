'use client';

export const defraColorClassnameBasedOnBandValue = (band: number) => {
    console.log(`BAND: ${band}`);
    if([1, 2, 3].includes(band)) {
        return `text-green-600 dark:text-green-400`;
    } else if([4, 5, 6].includes(band)) {
        return `text-amber-600 dark:text-amber-400`;
    } else if([7, 8, 9].includes(band)) {
        return `text-rose-600`;
    } else if([10].includes(band)) {
        return `text-purple-600 dark:text-purple-400`;
    } else {
        return ``
    }
}

