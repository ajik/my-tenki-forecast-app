
import { LRUCache } from "lru-cache";
import { SERVER_ERROR_MESSAGE, TOKEN_LIMIT } from "./constant";
import { GetEnvironmentVariableValue } from "./system.util";

const uniqueTokenPerInterval = GetEnvironmentVariableValue("NEXT_PUBLIC_UNIQUE_TOKEN_PER_INTERVAL", "number") as number;
const intervalInSecond = GetEnvironmentVariableValue("NEXT_PUBLIC_INTERVAL", "number") as number;

export const limiter = rateLimit({
    interval: (intervalInSecond || 60) * 1000, // 60 seconds
    uniqueTokenPerInterval: uniqueTokenPerInterval || 10, // Max 10 users per second
});

type Options = {
    uniqueTokenPerInterval?: number;
    interval?: number;
};

export default function rateLimit(options?: Options) {
    const limit = TOKEN_LIMIT || 20;
    const token = GetEnvironmentVariableValue("NEXT_PUBLIC_TOKEN_KEY");
    const tokenCache = new LRUCache({
        max: options?.uniqueTokenPerInterval || 500,
        ttl: options?.interval || 60000,
    });

    return {
        check: () =>
            new Promise<void>((resolve, reject) => {
                const tokenCount = (tokenCache.get(token) as number[]) || [0];
                if (tokenCount[0] === 0) {
                    tokenCache.set(token, tokenCount);
                }
                tokenCount[0] += 1;

                const currentUsage = tokenCount[0];
                const isRateLimited = currentUsage >= limit;

                return isRateLimited ? reject(SERVER_ERROR_MESSAGE.EM_0002.code) : resolve();
            }
        ),
        setHeader: (headers: any = {}) => {
            const tokenCount = (tokenCache.peek(token) as number[]) || [0];
            const currentUsage = tokenCount[0];
            
            const isRateLimited = currentUsage >= limit;
            const additionalHeaders = {
                "X-RateLimit-Limit": limit,
                "X-RateLimit-Remaining": isRateLimited ? 0 : limit - currentUsage,
            }
            if(headers) {
                headers = {
                    ...headers,
                    ...additionalHeaders
                }
            } else {
                headers = additionalHeaders
            }
            return headers;
        }
    };
}