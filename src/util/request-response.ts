import { limiter } from "./rate-limit";
import { GenerateErrorResponseInformation } from "./system.util";

export function SendResponse(
    data: any,
    status: number = 200,
    headers: any = null
) {
    if (!headers || headers == null) {
        headers = {
            "Content-Type": "application/json",
            Accept: "application/json",
        };
    }
    headers = limiter.setHeader(headers)
    return new Response(JSON.stringify(data, null, 4), {
        status: status,
        headers: headers,
    });
}

export function SendErrorResponse(data: string) {
    let headers = {
        "Content-Type": "application/json",
        Accept: "application/json",
    };

    headers = limiter.setHeader(headers)
    const error = GenerateErrorResponseInformation(data)

    return new Response(
        JSON.stringify(
            {
                status: "failure",
                error: error.message,
            },
            null,
            4
        ),
        {
            status: error.statusCode,
            headers: headers,
        }
    );
}