import { SERVER_ERROR_MESSAGE } from "./constant";

export const GetEnvironmentVariableValue = (
    variableName: string,
    dataType: "string" | "number" = "string"
) => {
    let value: number | string | undefined = process.env[variableName];
    if (value === undefined) {
        console.error(`${variableName} is not registered as env variable`)
        throw new Error(`Server Error: [Code:${SERVER_ERROR_MESSAGE.EM_0001.code}]! Please contact administrator`);
    };
    if (dataType === "number") {
        value = Number.parseInt(value);
    }

    return value;
};

interface ErrorResponseInformation {
    message: string,
    statusCode: number
}

export const GenerateErrorResponseInformation = (errorCode: string): ErrorResponseInformation => {
    let errorType = SERVER_ERROR_MESSAGE[errorCode as keyof typeof SERVER_ERROR_MESSAGE];
    let errorMessage = "";
    let responseCode = 500;
    if(errorType) {
        errorMessage = `ERROR [${errorType.code}]: ${errorType.message}`;
        responseCode = errorType.responseCode;
    } else {
        errorType = SERVER_ERROR_MESSAGE["EM_0000"];
        errorMessage = `ERROR [${errorType.code}]: ${errorType.message}, Reason: ${errorCode}`;
        responseCode = errorType.responseCode;
    }

    return {
        message: errorMessage,
        statusCode: responseCode,
    }

}

export const GetEndpointURL = () => {
    const baseUrl = GetEnvironmentVariableValue('NEXT_PUBLIC_API_BASE_URL');
    const apiVersion = GetEnvironmentVariableValue('NEXT_PUBLIC_API_VERSION');
    const url = `${baseUrl}/${apiVersion}`
    return url;
}