export type TemperatureUnitSimple = "F" | "C";
export type TemperatureUnit = "Fahrenheit" | "Celcius";