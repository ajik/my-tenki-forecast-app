// TODO: fix with 16 compass direction
export type WindDirection = "N" | "NNE" | "NE" | "ENE" | "E" | "ESE" | "SE" | "SSE" | "S" | "SSW" | "SW" | "WSW" | "W" | "WNW" | "NW" | "NNW";

interface WindDirectionDetail {
    type: WindDirection;
    remark: string;
}

const windDirectionInformations: WindDirectionDetail[] = [
    {
        type: "N",
        remark: "North",
    },
    {
        type: "NNE",
        remark: "North-Northeast",
    },
    {
        type: "NE",
        remark: "Northeast",
    },
    {
        type: "ENE",
        remark: "East-Northeast",
    },
    {
        type: "E",
        remark: "East",
    },
    {
        type: "ESE",
        remark: "East-Southeast",
    },
    {
        type: "SE",
        remark: "South East",
    },
    {
        type: "SSE",
        remark: "South-Southeast",
    },
    {
        type: "S",
        remark: "South",
    },
    {
        type: "SSW",
        remark: "South-southwest",
    },
    {
        type: "SW",
        remark: "Southwest",
    },
    {
        type: "WSW",
        remark: "West-southwest",
    },
    {
        type: "W",
        remark: "West",
    },
    {
        type: "WNW",
        remark: "West-northwest",
    },
    {
        type: "NW",
        remark: "Northwest",
    },
    {
        type: "NNW",
        remark: "North-northwest",
    }
]

export const getWindDirection = (code?: string | WindDirection) => {
    if(code === undefined) return '-';
    const windDirection = windDirectionInformations.find((e) => e.type === code);
    if(windDirection) return windDirection.remark;
    else return '-';
}